package com.adit.daftaritem.services;

import com.adit.daftaritem.model.Items;
import com.adit.daftaritem.model.Msg;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetService {
    @GET("/api/v1/get_data/")
    Call<List<Items>> getDataItems();

    @Headers("Content-Type: application/json")
    @POST("/api/v1/send_data/")
    Call<Msg> sendItem(@Body String body);
}
