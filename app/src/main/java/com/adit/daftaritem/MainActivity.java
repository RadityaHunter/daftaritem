package com.adit.daftaritem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.adit.daftaritem.model.Items;
import com.adit.daftaritem.model.Msg;
import com.adit.daftaritem.services.ApiClient;
import com.adit.daftaritem.services.GetService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    EditText etItem;
    Button btnSimpan, btnKirim;
    String valItems, data, valExtra;
    TextView tvItem, tvItemLabel;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        getExtraIntent();
        getItemsData();
        listerner();
    }
    private void getExtraIntent() {
        Bundle extras = getIntent().getExtras();
        //String userName;

        if (extras != null) {
            data = getIntent().getExtras().getString("VAL_ITEMS_SELECT", "");
            id = getIntent().getExtras().getInt("VAL_ID", 0);
            if (!data.equals("")) {
                btnSimpan.setVisibility(View.GONE);
                btnKirim.setVisibility(View.VISIBLE);
                tvItem.setVisibility(View.VISIBLE);
                tvItemLabel.setVisibility(View.VISIBLE);
                etItem.setVisibility(View.GONE);

                //data = extras.getString("VAL_ITEMS_SELECT"); // retrieve the data using keyName
                //id = extras.getInt("VAL_ID");
                tvItem.setText(String.valueOf(data));
            } else {
                btnSimpan.setVisibility(View.VISIBLE);
                etItem.setVisibility(View.VISIBLE);
                btnKirim.setVisibility(View.GONE);
                tvItem.setVisibility(View.GONE);
                tvItemLabel.setVisibility(View.GONE);
            }
            //userName = extras.getString("name");
            // and get whatever type user account id is
        }
    }

    private void listerner() {
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!valItems.equals("")) {
                    Intent intent = new Intent(getBaseContext(), DaftarItemTersimpan.class);
                    intent.putExtra("VAL_ITEMS", valItems);
                    startActivity(intent);
                }
            }
        });

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!data.equals("")) {
                    sendItemData();
                }
            }
        });
    }

    private void sendItemData() {
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id", id);
            paramObject.put("name", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<Msg> call = service.sendItem(paramObject.toString());


        call.enqueue(new Callback<Msg>() {
            @Override
            public void onResponse(Call<Msg> call, Response<Msg> response) {
                Toast.makeText(getBaseContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Msg> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getItemsData() {
        //etItem.setText("Test");
        GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<List<Items>> call = service.getDataItems();
        call.enqueue(new Callback<List<Items>>() {
            @Override
            public void onResponse(Call<List<Items>> call, Response<List<Items>> response) {
                //etItem.setText(response.body().get(0).getName());
                valItems = "";
                StringBuilder res = new StringBuilder();
                for (int i = 0; i < response.body().size(); i++) {
                    res.append(response.body().get(i).getName()).append(", ");
                    if (i == response.body().size() - 1) {
                        res.append(response.body().get(i).getName());
                    }
                }

                etItem.setText(res.toString());
                valItems = res.toString();
            }

            @Override
            public void onFailure(Call<List<Items>> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void initView() {
        etItem = findViewById(R.id.etItem);
        etItem.setKeyListener(null);
        btnSimpan = findViewById(R.id.btnSimpan);
        btnKirim = findViewById(R.id.btnKirim);
        tvItemLabel = findViewById(R.id.tvItemLabel);
        tvItem = findViewById(R.id.tvItem);
    }
}