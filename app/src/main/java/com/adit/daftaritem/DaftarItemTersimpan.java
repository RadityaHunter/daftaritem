package com.adit.daftaritem;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DaftarItemTersimpan extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spinner;
    Button btnSave;
    String valItemsNew;
    ArrayList<String> listOfString;
    String valItemSelect;
    int valID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_item_tersimpan);
        getExtraIntent();
        initView();
        showInSpinner();
        listerner();
    }
    private void listerner() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!valItemSelect.equals("")) {
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    intent.putExtra("VAL_ITEMS_SELECT", valItemSelect);
                    intent.putExtra("VAL_ID", valID + 1);
                    startActivity(intent);
                }
            }
        });
    }

    private void showInSpinner() {
        valItemSelect = "";

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> items = new ArrayList<String>();
        //categories.add("Pilih Item");
        items.addAll(listOfString);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setPrompt("Pilih Item");
    }


    private void getExtraIntent() {
        valItemsNew = getIntent().getExtras().getString("VAL_ITEMS", "");
        //Toast.makeText(getBaseContext(), valItemsNew, Toast.LENGTH_SHORT).show();
        splitData(valItemsNew);
    }

    private void initView() {
        spinner = findViewById(R.id.spinner);
        btnSave = findViewById(R.id.btnSave);
    }

    private void splitData(String valItemsNew) {
        //String csv = "Apple, Google, Samsung";

        // step one : converting comma separate String to array of String
        String[] elements = valItemsNew.split(",");

        // step two : convert String array to list of String
        List<String> fixedLenghtList = Arrays.asList(elements);

        // step three : copy fixed list to an ArrayList
        listOfString = new ArrayList<String>(fixedLenghtList);

        //System.out.println("list from comma separated String : " + listOfString.get(0));
        //System.out.println("size of ArrayList : " + listOfString.size());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();
        valItemSelect = adapterView.getItemAtPosition(i).toString();
        valID = i;
        // Showing selected spinner item
        Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}